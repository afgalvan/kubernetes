FROM openjdk:17-jdk-alpine

WORKDIR /app
COPY build/libs/hello-world-0.1-all.jar .
EXPOSE 9000
CMD [ "java", "-jar", "hello-world-0.1-all.jar" ]
