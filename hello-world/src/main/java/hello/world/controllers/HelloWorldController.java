package hello.world.controllers;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/java")
public class HelloWorldController {

    @Get(produces= MediaType.TEXT_HTML)
    public String index() {
        return "Hello from java!";
    }
}
