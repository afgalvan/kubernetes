import express from "express";

const app = express();

app.get("/node", (_, res) => res.send("Hello from node!"));
app.listen(process.env.PORT || 3000);
