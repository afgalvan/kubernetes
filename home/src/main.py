from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route("/")
def index():
    languages = ["go", "java", "node"]
    endpoints_url = {lang: f"{request.host_url}{lang}" for lang in languages}
    return jsonify({"endpoints": endpoints_url})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6000)
